{{- define "vcluster-init.path" -}}
{{- if hasKey . "$" -}}
  {{- include "vcluster-init._path" . -}}
{{- else -}}
  {{- include "vcluster-init._path" (dict "$" $) -}}
{{- end -}}
{{- end -}}

{{- define "vcluster-init._path" -}}
{{- $root := index . "$" -}}
{{- if not (hasSuffix ".yaml" $root.Template.Name) -}}
  {{- fail "Template files should have a '.yaml' extension" -}}
{{- end -}}
{{- $root.Template.Name | substr (len $root.Template.BasePath | add 1 | int) (len $root.Template.Name) -}}
{{- end -}}


{{- define "vcluster-init.dir" -}}
{{- if hasKey . "$" -}}
  {{- include "vcluster-init._dir" . -}}
{{- else -}}
  {{- include "vcluster-init._dir" (dict "$" $) -}}
{{- end -}}
{{- end -}}

{{- define "vcluster-init._dir" -}}
{{- $root := index . "$" -}}
{{- $parts := (include "vcluster-init._path" .) | dir | splitList "/" -}}
{{ range $key, $value := $parts }}
  {{- if and (not (hasPrefix "_" $value)) (ne $value ".") -}}
    {{- if ne $key 0 -}}/{{- end -}}
    {{- $value -}}
  {{- end -}}
{{- end -}}
{{- end -}}


{{- define "vcluster-init.file" -}}
{{- if hasKey . "$" -}}
  {{- include "vcluster-init._file" . -}}
{{- else -}}
  {{- include "vcluster-init._file" (dict "$" $) -}}
{{- end -}}
{{- end -}}

{{- define "vcluster-init._file" -}}
{{- include "vcluster-init._path" . | base -}}
{{- end -}}


{{/*
Expand the name of the chart.
*/}}
{{- define "vcluster-init.name" -}}
{{- if hasKey . "$" -}}
  {{- include "vcluster-init.name" . -}}
{{- else -}}
  {{- include "vcluster-init._name" (dict "$" $) -}}
{{- end -}}
{{- end }}

{{- define "vcluster-init._name" -}}
{{- $root := index . "$" -}}
{{- coalesce .name $root.Values.nameOverride $root.Chart.Name }}
{{- end }}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "vcluster-init.fullname" -}}
{{- if hasKey . "$" -}}
  {{- include "vcluster-init._fullname" . -}}
{{- else -}}
  {{- include "vcluster-init._fullname" (dict "$" $) -}}
{{- end -}}
{{- end -}}

{{- define "vcluster-init._fullname" -}}
{{- $root := index . "$" -}}
{{- $name := include "vcluster-init._name" . -}}
{{- if contains $name $root.Release.Name -}}
  {{- $name = "" -}}
{{- end -}}
{{- $component := include "vcluster-init._component" . -}}
{{- $addition := list $name $component .suffix | compact | join "-" -}}
{{- $max_fullname_len := 52 -}}{{- /* Cronjobs have a maximum resource name length of 52 characters */}}
{{- $max_name_len := sub $max_fullname_len 12 -}}{{- /* Leaving room for the base name */}}
{{- if gt ($addition | len) $max_name_len -}}
    {{- fail (printf "name '%s' exceeds max length of %d and found %d characters" $addition $max_name_len (len $addition)) -}}
{{- end -}}
{{- $base := $root.Release.Name -}}
{{- $base_override := (coalesce .fullname $root.Values.fullnameOverride) -}}
{{- if $base_override -}}
  {{- $addition = list $component .suffix | compact | join "-" -}}
  {{- if gt ($base_override | len) $max_fullname_len -}}
    {{- fail (printf "fullname override '%s' exceeds max length of %d and found %d characters" $base_override $max_fullname_len (len $base_override)) -}}
  {{- end -}}
  {{- $base = $base_override -}}
{{- end -}}
{{- $fullname := list $base $addition | compact | join "-" -}}
{{- if gt ($fullname | len) $max_fullname_len -}}
  {{- $base = $base | trunc ($addition | len | sub $max_fullname_len | add -1 | int) | trimSuffix "-" -}}
  {{- $fullname = list $base $addition | compact | join "-" -}}
{{- end -}}
{{- if not (regexMatch "^[A-Za-z].+" $fullname) -}}
  {{- fail (printf "fullname must begin with an alphabetic character but found '%s'" $fullname) -}}
{{- else if gt ($fullname | len) $max_fullname_len -}}
  {{- fail (printf "fullname '%s' exceeds max length of %d and found %d characters" $fullname $max_fullname_len ($fullname | len)) -}}
{{- end -}}
{{- $fullname -}}
{{- end }}


{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "vcluster-init.chart" -}}
{{- if hasKey . "$" -}}
  {{ include "vcluster-init._chart" . }}
{{- else -}}
  {{ include "vcluster-init._chart" (dict "$" $) }}
{{- end -}}
{{- end }}

{{- define "vcluster-init._chart" -}}
{{- $root := index . "$" -}}
{{- printf "%s-%s" $root.Chart.Name $root.Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}


{{- define "vcluster-init.component" -}}
{{- if hasKey . "$" -}}
  {{- include "vcluster-init._component" . -}}
{{- else -}}
  {{- include "vcluster-init._component" (dict "$" $) -}}
{{- end -}}
{{- end -}}

{{- define "vcluster-init._component" -}}
{{- $root := index . "$" -}}
{{- if hasKey . "component" -}}
  {{- .component -}}
{{- else -}}
  {{- $dir := include "vcluster-init._dir" . -}}
  {{- $file := regexReplaceAll "(?:([^\\.]+)\\.)?[^\\.]+\\.yaml" (include "vcluster-init._file" .) "${1}" -}}
  {{- (list $dir $file) | compact | join "-" | replace "/" "-" -}}
{{- end -}}
{{- end -}}


{{- define "vcluster-init.labels" -}}
{{- if hasKey . "$" -}}
  {{ include "vcluster-init._labels" . }}
{{- else -}}
  {{ include "vcluster-init._labels" (dict "$" $) }}
{{- end -}}
{{- end }}

{{- define "vcluster-init._labels" -}}
{{- $root := index . "$" -}}
helm.sh/chart: {{ include "vcluster-init._chart" . }}
{{ include "vcluster-init._selectorLabels" . }}
{{- if $root.Chart.AppVersion }}
app.kubernetes.io/version: {{ $root.Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ $root.Release.Service }}
{{- end }}


{{- define "vcluster-init.selectorLabels" -}}
{{- if hasKey . "$" -}}
  {{ include "vcluster-init._selectorLabels" . }}
{{- else -}}
  {{ include "vcluster-init._selectorLabels" (dict "$" $) }}
{{- end -}}
{{- end }}

{{- define "vcluster-init._selectorLabels" -}}
{{- $root := index . "$" -}}
app.kubernetes.io/name: {{ include "vcluster-init._name" . }}
app.kubernetes.io/instance: {{ $root.Release.Name }}
{{- $component := (include "vcluster-init._component" .) -}}
{{- if $component }}
app.kubernetes.io/component: {{ $component }}
{{- end }}
{{- end }}


{{/*
A checksum of templates and files given as arguments
*/}}
{{- define "vcluster-init.checksum" -}}
{{- if hasKey . "$" -}}
  {{ include "vcluster-init._checksum" . }}
{{- else -}}
  {{ include "vcluster-init._checksum" (dict "$" $) }}
{{- end -}}
{{- end -}}

{{- define "vcluster-init._checksum" -}}
{{- $root := index . "$" -}}
{{- $current_context := . -}}
{{- $files := "" -}}
{{- $_files := "" -}}
{{- $values := "" -}}
{{- if .file -}}
  {{- $files = include (list $root.Template.BasePath (include "vcluster-init._dir" $current_context) .file | compact | join "/") $root -}}
{{- end -}}
{{- if .files -}}
  {{- range .files -}}
    {{- $files = list $files (include (list $root.Template.BasePath (include "vcluster-init._dir" $current_context) . | compact | join "/") $root) | compact | join "\n" -}}
  {{- end -}}
{{- end -}}
{{- if .valuess -}}
  {{- $values = toYaml .values -}}
{{- end -}}
{{- list $files $values | join "\n" | sha256sum -}}
{{- end -}}


{{- define "vcluster-init.serviceAccountName" -}}
{{- if hasKey . "$" -}}
  {{ include "vcluster-init._serviceAccountName" . }}
{{- else -}}
  {{ include "vcluster-init._serviceAccountName" (dict "$" $ "component" "") }}
{{- end -}}
{{- end }}

{{- define "vcluster-init._serviceAccountName" -}}
{{- $root := index . "$" -}}
{{- $service_account := $root.Values.serviceAccount -}}
{{- if $service_account.create }}
  {{- $service_account.name | default (include "vcluster-init._fullname" .) }}
{{- else }}
  {{- $service_account.name | default "default" }}
{{- end }}
{{- end }}


{{- define "vcluster-init.image" -}}
{{- if hasKey . "$" -}}
  {{ include "vcluster-init._image" . }}
{{- else -}}
  {{ include "vcluster-init._image" (dict "$" $) }}
{{- end -}}
{{- end -}}

{{- define "vcluster-init._image" -}}
{{- $root := index . "$" -}}
{{- $dir := include "vcluster-init._dir" . -}}
{{- $repository := "" -}}
{{- $tag := "" -}}
{{- $root_image := $root.Values.image -}}
{{- if $dir -}}
  {{- $dir_image := (index $root.Values $dir).image -}}
  {{- $repository = (coalesce $dir_image.repository $root_image.repository) | required (printf "could not find image repository for '%s' because of missing value '%s.image.repository' or 'image.repository'" $dir $dir) -}}
  {{- $tag = (coalesce $dir_image.tag $root_image.tag $root.Chart.AppVersion) | required (printf "could not find image tag for '%s' because of missing value '%s.image.repository' or 'image.tag' or chart 'appVersion'" $dir $dir) -}}
{{- else -}}
  {{- $repository = $root_image.repository | required "could not find image repository because of missing value 'image.repository'" -}}
  {{- $tag = (coalesce $root_image.tag $root.Chart.AppVersion) | required "could not find image tag because of missing value 'image.tag' or chart 'appVersion'" -}}
{{- end -}}
{{- printf "%s:%s" $repository $tag -}}
{{- end -}}


{{/*
If the image tag contains an '.' it returns 'IfNotPresent' else 'Always'
*/}}
{{- define "vcluster-init.imagePullPolicy" -}}
{{- if hasKey . "$" -}}
  {{ include "vcluster-init._imagePullPolicy" . }}
{{- else -}}
  {{ include "vcluster-init._imagePullPolicy" (dict "$" $) }}
{{- end -}}
{{- end -}}

{{- define "vcluster-init._imagePullPolicy" -}}
{{- $root := index . "$" -}}
{{- $image := (index $root.Values (include "vcluster-init._dir" .)).image }}
{{- if $image.pullPolicy -}}
  {{- $image.pullPolicy -}}
{{- else if contains "." ((include "vcluster-init._image" .) | splitList ":" | last) -}}
  IfNotPresent
{{- else -}}
  Always
{{- end -}}
{{- end -}}


{{/*
Returns the result of an include defined as 'i' argument.
*/}}
{{- define "vcluster-init.subinclude" -}}
{{- $root := index . "$" | required "missing '$' argument in include" -}}
{{- $name := .i | required "missing 'i' argument in include" | splitList "." | first -}}
{{- $Chart := $root.Chart -}}
{{- $Values := $root.Values -}}
{{- range $root.Chart.Dependencies -}}
  {{- if eq .Name $name -}}
    {{- $Chart = . -}}
    {{- $Values = (index $root.Values $name) -}}
  {{- end -}}
{{- end -}}
{{- if .a -}}
  {{- include .i (merge (dict "$" (merge (unset (unset (deepCopy $root) "Chart") "Values") (dict "Chart" $Chart) (dict "Values" $Values))) .a) -}}
{{- else -}}
  {{- include .i (merge (unset (unset (deepCopy $root) "Chart") "Values") (dict "Chart" $Chart) (dict "Values" $Values)) -}}
{{- end -}}
{{- end -}}
