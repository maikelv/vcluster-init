FROM docker.io/library/python:3.10-alpine

WORKDIR /usr/app
RUN touch /.pdbhistory \
 && chown -R nobody:nogroup \
        /.pdbhistory \
        .

USER nobody

ENV PATH="/usr/app/venv/bin:${PATH}" \
    PYTHONUNBUFFERED="1" \
    VIRTUAL_ENV="/usr/app/venv"

RUN python -m venv ${VIRTUAL_ENV} \
 && pip install --no-cache-dir \
        black=="22.*" \
        flake8=="6.*" \
        flask=="2.*" \
        ipdb=="0.13.*" \
        isort \
        gunicorn=="20.*" \
        kubernetes

ENV FLASK_APP="src:app" \
    FLASK_RUN_PORT="8000"

EXPOSE 8000
CMD ["gunicorn", "--bind=0.0.0.0", "src:app"]

# See .dockerignore for the copied files (allow list)
COPY . .
