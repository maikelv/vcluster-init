import base64
import random
import re
from datetime import datetime, timezone
from time import sleep

import yaml
from flask import (
    Flask,
    Response,
    make_response,
    redirect,
    render_template,
    render_template_string,
    request,
    send_from_directory,
)
from kubernetes import client, config, utils

config.load_incluster_config()

app = Flask(__name__)
app.config.from_prefixed_env()
app.config["SESSION_COOKIE_SECURE"] = True
app.config["SESSION_COOKIE_HTTPONLY"] = True

TITLE = app.config.get("title", "vCluster init")

app.config["admin_pin"] = str(app.config["admin_pin"])
app.config["pin"] = str(app.config["pin"])

MANIFESTS_NAMESPACE_TPL = open(
    "/var/run/templates-manifests/namespace.yaml", "r"
).read()
MANIFESTS_APPLICATION_TPL = open(
    "/var/run/templates-manifests/application.yaml", "r"
).read()

if "image_pull_secret_source" in app.config:
    MANIFESTS_IMAGE_PULL_SECRET_TPL = open(
        "/var/run/templates-manifests/image-pull.secret.yaml", "r"
    ).read()

if "tls_secret_source" in app.config:
    MANIFESTS_TLS_SECRET_TPL = open(
        "/var/run/templates-manifests/tls.secret.yaml", "r"
    ).read()


if app.config["DEBUG"]:
    if app.config["admin_pin"] != "0":
        print(f"Admin PIN: {app.config['admin_pin']}")
    print(f"App PIN: {app.config['pin']}")

slug_re = re.compile("[^a-zA-Z]")


def is_authorized():

    if app.config["pin"] == "0":
        return True

    if request.authorization:
        pin = request.authorization.password
    else:
        pin = request.cookies.get(f"pin-{app.config['cookie_name_suffix']}")

    if pin == app.config["pin"]:
        return True

    if pin == app.config["admin_pin"]:
        return True

    return False


def is_authorized_admin():
    if app.config["admin_pin"] == "0":
        return is_authorized()

    if request.authorization:
        pin = request.authorization.password
    else:
        pin = request.cookies.get(f"pin-{app.config['cookie_name_suffix']}")

    if pin == app.config["admin_pin"]:
        return True

    return False


def is_logged_in():
    if app.config["pin"] == "0":
        return False
    return is_authorized()


@app.get("/healthz/")
def healthz():
    response = make_response("ok", 200)
    response.mimetype = "text/plain"
    return response


@app.route("/statics/<path:path>")
def send_static_file(path):
    return send_from_directory("statics", path)


@app.get("/")
def root():
    if not is_authorized():
        return redirect("/login/")

    id = request.cookies.get("id")
    first_name = request.cookies.get("first-name")
    if not id:
        error = request.cookies.get("error")
        response = Response(
            render_template(
                "index.html",
                title=TITLE,
                show_logout=is_logged_in(),
                error=error if error else "",
                first_name=first_name if first_name else "",
                is_admin=is_authorized_admin(),
            )
        )
        response.delete_cookie("error", httponly=True)
        return response

    scheme = request.headers.get("X-Forwarded-Proto", request.scheme)
    host = request.headers.get("X-Forwarded-Host", request.host)
    return render_template(
        "cluster.html",
        title=TITLE,
        show_logout=is_logged_in(),
        id=id,
        base_url=f"{scheme}://{host}",
        pin=app.config["pin"],
        kubeconfig=get_kubeconfig(id),
        is_admin=is_authorized_admin(),
        delete_uri="/delete-cluster/",
    )


@app.get("/login/")
def login():
    if not is_authorized():
        return render_template("login.html", title=TITLE, show_logout=is_logged_in())

    return redirect("/")


@app.post("/login/")
def post_login():
    pin = request.values.get("pin")
    if pin not in [app.config["pin"], app.config["admin_pin"]]:
        error = "Invalid PIN" if pin else "Provide a PIN"
        sleep(2)
        return render_template(
            "login.html", title=TITLE, show_logout=is_logged_in(), error=error
        )

    if pin == app.config["admin_pin"]:
        response = redirect("/admin/")
    else:
        response = redirect("/")

    response.set_cookie(f"pin-{app.config['cookie_name_suffix']}", pin, httponly=True)
    return response


@app.get("/logout/")
def logout():
    response = redirect("/")
    response.delete_cookie(f"pin-{app.config['cookie_name_suffix']}", httponly=True)
    return response


@app.post("/create-cluster/")
def create_cluster():
    if not is_authorized():
        return redirect("/login/")

    response = redirect("/")
    if request.cookies.get("id"):
        return response

    first_name = request.values.get("first-name")
    if not first_name:
        return response

    response.set_cookie("first-name", first_name, httponly=True)
    first_name_slug = slug_re.sub("", first_name)[0:15].lower()
    id = first_name_slug + "-" + "".join(random.choices("0123456789abcdef", k=6))

    api = client.CoreV1Api()
    for cluster in api.list_namespace().items:
        if not cluster.metadata.name.startswith("vcluster-"):
            continue
        if cluster.status.phase != "Active":
            continue
        nameparts = cluster.metadata.name.split("-")
        cluster_first_name = "-".join(nameparts[1:-1])
        if cluster_first_name == first_name_slug:
            response.set_cookie("error", "First name already taken", httponly=True)
            return response

    api_client = client.ApiClient()
    custom_objects_api = client.CustomObjectsApi()

    namespace_manifest = yaml.safe_load(
        render_template_string(
            MANIFESTS_NAMESPACE_TPL,
            id=id,
            first_name=request.cookies.get("first-name"),
        )
    )
    utils.create_from_dict(api_client, data=namespace_manifest, verbose=True)

    if "image_pull_secret_source" in app.config:
        image_pull_secret = api.read_namespaced_secret(
            app.config["image_pull_secret_source"]["name"],
            app.config["image_pull_secret_source"]["namespace"],
        )
        new_image_pull_secret_manifest = yaml.safe_load(
            render_template_string(
                MANIFESTS_IMAGE_PULL_SECRET_TPL,
                id=id,
            )
        )
        new_image_pull_secret_manifest["data"] = image_pull_secret.data
        utils.create_from_dict(
            api_client, data=new_image_pull_secret_manifest, verbose=True
        )

    if "tls_secret_source" in app.config:
        tls_secret = api.read_namespaced_secret(
            app.config["tls_secret_source"]["name"],
            app.config["tls_secret_source"]["namespace"],
        )
        new_tls_secret_manifest = yaml.safe_load(
            render_template_string(
                MANIFESTS_TLS_SECRET_TPL,
                id=id,
            )
        )
        new_tls_secret_manifest["data"] = tls_secret.data
        utils.create_from_dict(api_client, data=new_tls_secret_manifest, verbose=True)

    application_manifest = yaml.safe_load(
        render_template_string(MANIFESTS_APPLICATION_TPL, id=id)
    )
    custom_objects_api.create_namespaced_custom_object(
        group=application_manifest["apiVersion"].split("/")[0],
        version=application_manifest["apiVersion"].split("/")[1],
        plural="applications",
        namespace=application_manifest["metadata"]["namespace"],
        body=application_manifest,
    )

    response.set_cookie("id", id, httponly=True)
    return response


@app.get("/config")
def kubeconfig():
    if request.authorization:
        id = request.authorization.username
    else:
        id = request.cookies.get("id")

    if not id:
        return (
            Response("# Cluster ID not provided", 403)
            if request.authorization
            else redirect("/")
        )

    if not is_authorized():
        return (
            Response("# Invalid PIN", 403)
            if request.authorization
            else redirect("/login/")
        )

    return get_kubeconfig_response(id)


def get_kubeconfig_response(id):
    if kubeconfig := get_kubeconfig(id):
        response = Response(
            kubeconfig,
            mimetype="text/plain",
            headers={"Content-disposition": "attachment; filename=config"},
        )
    else:
        response = Response("# Invalid cluster ID", 403)
    return response


def get_kubeconfig(id):
    api = client.CoreV1Api()
    application_manifest = yaml.safe_load(
        render_template_string(MANIFESTS_APPLICATION_TPL, id=id)
    )
    kube_api_host = [
        p["value"]
        for p in application_manifest["spec"]["source"]["helm"]["parameters"]
        if p["name"] == "ingress.host"
    ][0]
    try:
        kubeconfig = (
            base64.b64decode(
                api.read_namespaced_secret(
                    "vc-vcluster",
                    application_manifest["spec"]["destination"]["namespace"],
                ).data["config"]
            )
            .decode("utf-8")
            .replace("localhost:8443", kube_api_host)
        )
    except client.exceptions.ApiException:
        kubeconfig = ""
    return kubeconfig


@app.get("/delete-cluster/")
def delete_cluster():
    if not is_authorized():
        return redirect("/login/")

    response = redirect("/")
    if id := request.cookies.get("id"):
        namespace_manifest = yaml.safe_load(
            render_template_string(MANIFESTS_NAMESPACE_TPL, id=id)
        )
        application_manifest = yaml.safe_load(
            render_template_string(MANIFESTS_APPLICATION_TPL, id=id)
        )
        api = client.CoreV1Api()
        custom_objects_api = client.CustomObjectsApi()
        try:
            custom_objects_api.delete_namespaced_custom_object(
                group=application_manifest["apiVersion"].split("/")[0],
                version=application_manifest["apiVersion"].split("/")[1],
                plural="applications",
                name=application_manifest["metadata"]["name"],
                namespace=application_manifest["metadata"]["namespace"],
            )
        except Exception as e:
            print(e)

        try:
            api.delete_namespace(namespace_manifest["metadata"]["name"])
        except Exception as e:
            print(e)
        response.delete_cookie("id", httponly=True)

    return response


@app.get("/admin/")
def admin():
    if not is_authorized_admin():
        return redirect("/login/")

    api = client.CoreV1Api()
    clusters = []
    for cluster in api.list_namespace().items:
        if not cluster.metadata.name.startswith("vcluster-"):
            continue

        nameparts = cluster.metadata.name.split("-")
        clusters.append(
            {
                "id": "-".join(nameparts[1:]),
                "status": cluster.status.phase,
                "first_name": cluster.metadata.annotations.get(
                    "vcluster-init/first-name"
                )
                if cluster.metadata.annotations
                else " ".join(nameparts[1:-1]),
                "age": pretty_date(cluster.metadata.creation_timestamp),
            }
        )
    response = Response(
        render_template(
            "admin.html",
            title=TITLE,
            show_logout=is_logged_in(),
            clusters=clusters,
            is_admin=True,
        )
    )
    response.delete_cookie("id", httponly=True)
    return response


@app.get("/admin/<id>/")
def admin_cluster(id):
    if not is_authorized_admin():
        return redirect("/login/")

    scheme = request.headers.get("X-Forwarded-Proto", request.scheme)
    host = request.headers.get("X-Forwarded-Host", request.host)
    return render_template(
        "cluster.html",
        title=TITLE,
        show_logout=is_logged_in(),
        id=id,
        base_url=f"{scheme}://{host}",
        pin=app.config.get("pin"),
        kubeconfig=get_kubeconfig(id),
        delete_uri=f"/admin/{id}/delete/",
        is_admin=True,
    )


@app.get("/admin/<id>/delete/")
def admin_delete_cluster(id):
    if not is_authorized_admin():
        return redirect("/login/")

    response = redirect("/admin/")
    namespace_manifest = yaml.safe_load(
        render_template_string(MANIFESTS_NAMESPACE_TPL, id=id)
    )
    application_manifest = yaml.safe_load(
        render_template_string(MANIFESTS_APPLICATION_TPL, id=id)
    )
    api = client.CoreV1Api()
    custom_objects_api = client.CustomObjectsApi()
    try:
        custom_objects_api.delete_namespaced_custom_object(
            group=application_manifest["apiVersion"].split("/")[0],
            version=application_manifest["apiVersion"].split("/")[1],
            plural="applications",
            name=application_manifest["metadata"]["name"],
            namespace=application_manifest["metadata"]["namespace"],
        )
    except Exception as e:
        print(e)
    try:
        api.delete_namespace(namespace_manifest["metadata"]["name"])
    except Exception as e:
        print(e)

    return response


def pretty_date(delta=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    now = datetime.now(timezone.utc)
    diff = now - delta
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ""

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(second_diff // 60) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(second_diff // 3600) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff // 7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff // 30) + " months ago"
    return str(day_diff // 365) + " years ago"
